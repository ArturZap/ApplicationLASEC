﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApplicationLASEC
{
    public partial class Form1 : Form
    {
        static ClasePuente Puente = new ClasePuente(9);  // Creación del objeto
        int p1;
        int bitIde;
        private void CrearDataGrid()
        {

            dataGridView1.Columns.Add("IdVehiculo", "IdVehiculo");
            dataGridView1.Columns.Add("IdVehiculoEntraIzq", "VehiculoEntraIzq");
            dataGridView1.Columns.Add("IdVehiculoPasaIzqDer", "VehiculoPasaIzqDer");
            dataGridView1.Columns.Add("IdVehiculoSaleDer", "VehiculoSaleDer");
            dataGridView1.Columns.Add("IdCoche", "IdCoche");
            dataGridView1.Columns.Add("IdCocheEntraDer", "CocheEntraDer");
            dataGridView1.Columns.Add("IdCochePasaDerIzq", "CochePasaDerIzq");
            dataGridView1.Columns.Add("IdCocheSaleIzq", "CocheSaleIzq");
            
        }
        private void MostrarDataGridView()
        {
            ClaseVehiculo Vehiculo;  // Declaración de un objeto local
            dataGridView1.Rows.Clear();  // Limpia los renglones
            dataGridView1.Columns.Clear();  // Limpia las columnas
            CrearDataGrid();  // Invoca el método para crear el dataGridView1
            
            for (int celda = 0; celda <= Puente.Top - 1; celda++)
            {
                Vehiculo = Puente.Consultar(celda);
                dataGridView1.Rows.Add(Vehiculo.IdVehiculo, Vehiculo.IdVehiculoEntraIzq, Vehiculo.IdVehiculoPasaIzqDer, Vehiculo.IdVehiculoSaleDer, Vehiculo.IdCoche, Vehiculo.IdCocheEntraDer, Vehiculo.IdCochePasaDerIzq, Vehiculo.IdCocheSaleIzq);
                    // Despliega los datos del Puente en el dataGridView1
               
            }
        }
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            MostrarDataGridView();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClaseVehiculo Vehiculo = new ClaseVehiculo();  // Creación del objeto Vehiculo
            

            
            Vehiculo.IdVehiculo = double.Parse(textBox1.Text);
            
            Vehiculo.IdCoche = double.Parse(textBox2.Text);
            
            Vehiculo.IdVehiculoEntraIzq = 1;
            
                        
            if (Puente.Insertar(Vehiculo))
            {
                if (Vehiculo.IdVehiculo >= 1)
                {
                    Vehiculo.IdVehiculoPasaIzqDer = 1;
                    Vehiculo.IdVehiculoSaleDer = 1;
                    
                        if (Vehiculo.IdVehiculoSaleDer >= 1)
                        {
                            Vehiculo.IdCocheEntraDer = 1;
                        }
                    
                        if(Vehiculo.IdCocheEntraDer >= 1)
                        {
                            Vehiculo.IdCochePasaDerIzq = 1;
                        }
                    
                        if(Vehiculo.IdCochePasaDerIzq >= 1)
                        {
                            Vehiculo.IdCocheSaleIzq = 1;
                        }
                    MessageBox.Show("Vehiculo insertado en el arreglo(Puente):\n\nidVehiculoIzqDer: " + Vehiculo.IdVehiculo.ToString() + "\nEntraIzq: " + Vehiculo.IdVehiculoEntraIzq.ToString() + "\nPasaIzqDer: " + Vehiculo.IdVehiculoPasaIzqDer.ToString() + "\nSaleDer: " + Vehiculo.IdVehiculoSaleDer.ToString() + "\nidVehiculoDerIzq: " + Vehiculo.IdCoche.ToString() + "\nEntraDer: " + Vehiculo.IdCocheEntraDer.ToString() + "\nPasaDerIzq: " + Vehiculo.IdCochePasaDerIzq.ToString() + "\nSaleIzq: " + Vehiculo.IdCocheSaleIzq.ToString(), "INSERCIÓN EXITOSA", MessageBoxButtons.OK, MessageBoxIcon.Information);        
                }
            }
            else
                MessageBox.Show("No se pudo insertar, vehiculo ya existe el Puente", "E R R O R", MessageBoxButtons.OK);

            MostrarDataGridView();  // Actualiza los datos de los Puentees en el dataGridView1
            textBox1.Clear();  // Limpia los cuadros de texto
            textBox2.Clear();
            textBox1.Focus();
       }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();  // Termina la ejecución del programa
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
