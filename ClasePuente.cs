﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationLASEC
{
    class ClasePuente
    {
        private int top;  // Variable para contar la cantidad de celdas ocupadas
        private readonly int Max;  // Tamaño del arreglo

        private ClaseVehiculo[] Arreglo;  // Declaracion del arreglo de objetos

        public int Top
        {
            get
            {
                return (top);
            }
        }
        public ClasePuente(int m)  // Constructor
        {
            Max = m;
            top = 0;
            Arreglo = new ClaseVehiculo[Max];  // Creación del arreglo de tamaño Max
        }
        public bool PuenteLleno()  // Método para detectar si el arreglo está lleno
        {
            if (top == Max)
                return (true);
            else
                return (false);
        }
        public bool PuenteVacio()  // Método para detectar si el arreglo está vacío
        {
            if (top == 0)
                return (true);
            else
                return (false);
        }
        public bool Insertar(ClaseVehiculo Dato) // Método para insertar un dato en el arreglo
        {
            if (!PuenteLleno())  // Si no está lleno ...
            {
                for (int i = 0; i < top; i++)
                {
                    if (Arreglo[i].IdVehiculo == Dato.IdVehiculo) 
                        return (false);  // No se inserta el dato (duplicado)
                }

                Arreglo[top] = Dato;  // Se inserta el dato en el arreglo
                top++;  // Se incrementa la cantidad de datos del arreglo
                return (true);  // Inserción exitosa !!!
            }
            else
                return (false);  // No se inserta el dato (arreglo lleno)
        }
        public ClaseVehiculo Consultar(int celda)
        {
            if (celda >= 0 && celda < Top)
                return (Arreglo[celda]); // Devuelve el objeto almacenado en la celda del Arreglo
            else
                return (null);
        }
        public bool PrioridadVehicular(ClaseVehiculo Dato)  // Método para detectar si el arreglo está vacío
        {
            if (top <= 1)  // Si está lleno ...
                return (false);
            else
                return (true);
        } 

    }
}
