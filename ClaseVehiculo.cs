﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationLASEC
{
    class ClaseVehiculo
    {
        private double idVehiculo;
        private double idVehiculoEntraIzq;
        private double idVehiculoPasaIzqDer;
        private double idVehiculoSaleDer;
        private double idCoche;
        private double idCocheEntraDer;
        private double prioridadVehicular;
        private double idCochePasaDerIzq;
        private double idCocheSaleIzq;
        
        public double IdVehiculo
        {
            get
            {
                return (idVehiculo);
            }
            set
            {
                idVehiculo = value;
            }
        }
        public double IdVehiculoEntraIzq
        {
            get
            {
                return (idVehiculoEntraIzq);
            }
            set
            {
                idVehiculoEntraIzq = value;
            }
        }
        public double IdVehiculoPasaIzqDer
        {
            get
            {
                return (idVehiculoPasaIzqDer);
            }
            set
            {
                idVehiculoPasaIzqDer = value;
            }
        }
        public double IdVehiculoSaleDer
        {
            get
            {
                return (idVehiculoSaleDer);
            }
            set
            {
                idVehiculoSaleDer = value;
            }
        }
        public double IdCoche
        {
            get
            {
                return (idCoche);
            }
            set
            {
                idCoche = value;
            }
        }
        public double IdCocheEntraDer
        {
            get
            {
                return (idCocheEntraDer);
            }
            set
            {
                idCocheEntraDer = value;
            }
        }

        public double IdCochePasaDerIzq
        {
            get
            {
                return (idCochePasaDerIzq);
            }
            set
            {
                idCochePasaDerIzq = value;
            }
        }
        public double IdCocheSaleIzq
        {
            get
            {
                return (idCocheSaleIzq);
            }
            set
            {
                idCocheSaleIzq = value;
            }
        }
    }
}
